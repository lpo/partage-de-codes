Pour faire suite à un besoin du "groupe datas" du réseau LPO AuRA, ce dépot a été mis en place pour partager divers scripts de codes pouvant être utilses pour d'autres. 


https://framagit.org/lpo/partage-de-codes/


Deux possibilités s'offrent à vous:

* Pour un script complexe faisant appel à des ressource externes comme des jeux de donnée test par exemple, privilégier un ajout du code directement dans le dépot (c'est un outil de versionning qui permet le suivi dans le temps des codes (scripts R, Python, Jupyter (en R ou Python), jeux de données csv, etc). Pour cela, j'ai créé un dossier par grand type de language. Il faut a cet effet soit charger les fichiers à la main, soit utiliser l'éditeur, soit cloner le dépot et proposer une mise à jour.
* Pour les simple scripts, utiliser les "extraits de code" ou "snippets" ici avec quelques exemples (dont des fichiers Jupyter Notebook qu'il met automatiquement en forme ;) Pour les snippets, pensez à faire des description explicites avec des mots clés explicites (facilite la recherche) à préciser dans le titre, entre crochets, le language du script, qui permet de plus facilement s'y retrouver. https://framagit.org/lpo/partage-de-codes/snippets