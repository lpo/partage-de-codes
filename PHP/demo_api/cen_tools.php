<?php


/**
 * Return all the taxo groups the portal should show according to the agreement
 * @return array
 */
function get_taxo_groups() {
    return array(
        1, // Birds
        2, // Bats
        3, // Mammals
        4, // Sea mammals
        6, // Reptiles
        7  // Amphibians
    );
}


/**
 * Return the visionature databases list
 *
 * @return array
 */
function get_hosts() {
    global $db;

    $q = $db->query('SELECT * FROM public.vm_sources WHERE is_used IS True');
    return $q->fetchAll();
}


/**
 * Return a php array biolovision_id => cd_nom for TAXREF conversion
 * @return array
 */
function get_species_sheet() {
    global $db, $cache_species_sheet;

    if(is_array($cache_species_sheet))
        return $cache_species_sheet;

    $q = $db->query('SELECT biolovision_id, cd_nom FROM public.vm_species_sheet');
    $convert = array();

    while($line = $q->fetch(PDO::FETCH_ASSOC)) {
        $convert[$line['biolovision_id']] = $line['cd_nom'];
    }

    $cache_species_sheet = $convert;
    return $convert;
}


/**
 * Return the insee code, the_geom_point, and geojson for the given coordinates
 *
 * @param $lon
 * @param $lat
 * @return array|bool
 */
function get_area_info($lon, $lat) {
    global $db, $cache_lon_lat;

    //Checking cache for reteiving data
    if(!is_array($cache_lon_lat))
        $cache_lon_lat = array();

    if(array_key_exists($lon, $cache_lon_lat)) {
        if(array_key_exists($lat, $cache_lon_lat[$lon]))
            return $cache_lon_lat[$lon][$lat];
    }

    //Not in cache, retrieving information from database
    $data = [
        'insee' => '',
        'the_geom_point' => '',
        'geojson_point' => ''
    ];

    $point = "POINT($lon $lat)";

    $q = $db->prepare("SELECT
                      code,
                      type,
                      ST_AsText(ST_Centroid(shape)) as center,
                      ST_X(ST_Transform(ST_Centroid(shape), 4326)) as lon,
                      ST_Y(ST_Transform(ST_Centroid(shape), 4326))  as lat
                      FROM public.vm_areas
                      WHERE ST_Contains(shape, ST_Transform(ST_GeomFromText(:pt, 4326), 3857))");
    $q->bindParam('pt', $point);
    $q->execute();

    while ($line = $q->fetch(PDO::FETCH_ASSOC)) {
        if ($line['type'] == 'GRID') {
            $data['the_geom_point'] = $line['center'];
            $data['geojson_point'] = array(
                'id' => $line['code'],
                'type' => 'Feature',
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array($line['lon'], $line['lat'])
                ),
                'properties' => array(
                    'name' => $line['code']
                )
            );

            $data['geojson_point'] = json_encode($data['geojson_point']);
        } else {
            $data['insee'] = $line['code'];
        }
    }

    if(!empty($data['geojson_point']) && !empty($data['the_geom_point']) && !empty($data['insee'])) {
        $cache_lon_lat[$lon][$lat] = $data;
        return $data;
    }
    else if(!empty($data['geojson_point']) && !empty($data['the_geom_point']) && empty($data['insee'])) {
        $q = $db->prepare("SELECT code, ST_Distance(shape, ST_Transform(ST_GeomFromText(:pt, 4326), 3857)) as distance
			FROM public.vm_areas
			WHERE ST_DWithin(shape, ST_Transform(ST_GeomFromText(:pt, 4326), 3857), 5000) AND type = 'COMMUNE'
			ORDER BY distance ASC
			LIMIT 1");
		$q->bindParam('pt', $point);
		$q->execute();

		while ($line = $q->fetch(PDO::FETCH_ASSOC)) {
			log_info('Sighting position meget from closedst commune : PT ' . $point . ', INSEE : ' . $line['code']);
			$data['insee'] = $line['code'];
			$cache_lon_lat[$lon][$lat] = $data;
			return $data;
		}
	}
	
    return false;
}


function insert_sighting($source, $s) {
    global $db;

    //Extracting info from array
    $id = $source . $s['observers'][0]['id_sighting'];
    $validation = array_key_exists('admin_hidden', $s['observers'][0]) && $s['observers'][0]['admin_hidden'] > 0 ? false : true ;
    $observer = $s['observers'][0]['anonymous'] ?  'Anonyme' :  $s['observers'][0]['name'];
    $lon = $s['observers'][0]['coord_lon'];
    $lat = $s['observers'][0]['coord_lat'];
    $altitude = $s['observers'][0]['altitude'];
    $count = $s['observers'][0]['count'];
    $estimation_code = $s['observers'][0]['estimation_code'];
    $date = date('Y-m-d', $s['date']['@timestamp']);
    $specie = $s['species']['@id'];
    $species_sheet = get_species_sheet();

    if(!$validation) {
        log_info('Sighting ' . $s['observers'][0]['id_sighting'] . ' is not valid');
        return false;
    }

    if(array_key_exists($specie, $species_sheet))
        $specie = $species_sheet[$specie];
    else {
        log_info('Could not find specie TAXREF info for specie ' . $s['species']['@id'] . ',  ' . $s['species']['name']);
        return 0;
    }

    //Specie filter
    $species_filter = array(143, 153, 41, 330, 169);
    if(array_key_exists('atlas_code', $s['observers'][0]) && $s['observers'][0]['atlas_code']['#text'] > 1 && in_array($specie, $species_filter)) {
        log_info('Filter breeding ' . $s['species']['@id'] . ',  ' . $s['species']['name']);
        return 0;
    }

    //Remove absence data
    if($estimation_code == 'EXACT_VALUE' && $count < 1)
        return false;

    $area = get_area_info($lon, $lat);
    if(!$area) {
        log_info('Could not find area info for coords (' . $lat . ' ' . $lon . ')');
        return 0;
    }

    $the_geom_point = $area['the_geom_point'];
    $geojson_point = $area['geojson_point'];
    $insee = $area['insee'];

    $q = $db->prepare('INSERT INTO
                  vm_observations(
                    id_observation,
                    cd_nom,
                    insee,
                    dateobs,
                    date_insert,
                    date_update,
                    observateurs,
                    donnee_publique,
                    id_source,
                    validation,
                    the_geom_point,
                    geojson_point,
                    effectif_total,
                    altitude_retenue,
                    diffusable
                  )
                  VALUES(
                      :id_observation,
                      :specie,
                      :insee,
                      :date_observation,
                      NOW(),
                      NULL,
                      :observer,
                      FALSE,
                      :id_source,
                      TRUE,
                      ST_GeomFromText(:the_geom_point, 3857),
                      :geojson_point,
                      :count,
                      :altitude,
                      TRUE
                    )');


    $q->bindParam('id_observation', $id);
    $q->bindParam('specie', $specie);
    $q->bindParam('insee', $insee);
    $q->bindParam('date_observation', $date);
    $q->bindParam('observer', $observer);
    $q->bindParam('id_source', $source);
    $q->bindParam('the_geom_point', $the_geom_point);
    $q->bindParam('geojson_point', $geojson_point);
    $q->bindParam('count', $count);
    $q->bindParam('altitude', $altitude);
    $q->execute();
    return $id;
}

function update_sighting($source, $s) {
    global $db;

    //Extracting info from array
    $id = $source . $s['observers'][0]['id_sighting'];

    $q = $db->prepare("SELECT count(id_observation) as cnt FROM vm_observations WHERE id_observation = :id");
    $q->bindParam('id', $id);
    $q->execute();
    if($q->fetchColumn() < 1)
        return insert_sighting($source, $s);

    $validation = array_key_exists('admin_hidden', $s['observers'][0]) && $s['observers'][0]['admin_hidden'] > 0 ? false : true ;
    $observer = $s['observers'][0]['anonymous'] ?  'Anonyme' :  $s['observers'][0]['name'];
    $lon = $s['observers'][0]['coord_lon'];
    $lat = $s['observers'][0]['coord_lat'];
    $altitude = $s['observers'][0]['altitude'];
    $count = $s['observers'][0]['count'];
    $estimation_code = $s['observers'][0]['estimation_code'];
    $date = date('Y-m-d', $s['date']['@timestamp']);
    $specie = $s['species']['@id'];
    $species_sheet = get_species_sheet();

    if(!$validation) {
        log_info('Sighting ' . $s['observers'][0]['id_sighting'] . ' is not valid');
        return false;
    }

    if(array_key_exists($specie, $species_sheet))
        $specie = $species_sheet[$specie];
    else {
        log_info('Could not find specie TAXREF info for specie ' . $s['species']['@id'] . ',  ' . $s['species']['name']);
        return 0;
    }

    //Specie filter
    $species_filter = array(143, 153, 41, 330, 169);
    if(array_key_exists('atlas_code', $s['observers'][0]) && $s['observers'][0]['atlas_code']['#text'] > 1 && in_array($specie, $species_filter)) {
        log_info('Filter breeding ' . $s['species']['@id'] . ',  ' . $s['species']['name']);
        return 0;
    }

    //Remove absence data
    if($estimation_code == 'EXACT_VALUE' && $count < 1)
        return false;

    $area = get_area_info($lon, $lat);
    if(!$area) {
        log_info('Could not find area info for coords (' . $lat . ' ' . $lon . ')');
        return 0;
    }

    $the_geom_point = $area['the_geom_point'];
    $geojson_point = $area['geojson_point'];
    $insee = $area['insee'];

    $q = $db->prepare('UPDATE
                  vm_observations
                  SET
                    id_observation = :id_observation,
                    cd_nom = :specie,
                    insee = :insee,
                    dateobs = :date_observation,
                    date_update = NOW(),
                    observateurs = :observer,
                    donnee_publique = FALSE,
                    id_source = :id_source,
                    validation = TRUE,
                    the_geom_point = ST_GeomFromText(:the_geom_point, 3857),
                    geojson_point = :geojson_point,
                    effectif_total = :count,
                    altitude_retenue = :altitude,
                    diffusable = TRUE
                    WHERE id_observation = :id_observation');


    $q->bindParam('id_observation', $id);
    $q->bindParam('specie', $specie);
    $q->bindParam('insee', $insee);
    $q->bindParam('date_observation', $date);
    $q->bindParam('observer', $observer);
    $q->bindParam('id_source', $source);
    $q->bindParam('the_geom_point', $the_geom_point);
    $q->bindParam('geojson_point', $geojson_point);
    $q->bindParam('count', $count);
    $q->bindParam('altitude', $altitude);
    $q->execute();
    return $id;
}

function delete_sighting($s, $ids) {
    global $db;

    if(count($ids) < 1)
        return false;

    foreach($ids as $key=>$id)
        $ids[$key] = intval($s . $id);

    $db->query("DELETE FROM vm_observations WHERE id_observation IN(" . implode(',', $ids) . ") ");
}