<?php
//Setting up to 10 minutes for security
set_time_limit (600);
ini_set("memory_limit","1G");

header('Content-Type: text/html; charset=utf-8');
include_once ("local.php");
include_once ("curl_tools.php");

if(!is_file('db_sync.log'))
    fopen("db_sync.log", "w") or die("Error : Unable to create log file.");

log_info("************** start db_update **************");

//Setting up the file to store json with last update date
if(!is_file($filename)) {
    fopen($filename, "w") or die("Error : Couldn't set up file to store json. Please check writing permissions.");
    //Setting last call to know by default to prevent doing a dummy /diff for millions of data.
    file_put_contents($filename, '{last_call:' . date('H:i:s d.m.Y') . '}');
}

log_info("Getting taxo_groups...");

$user_data['user_pw']  = encode_password($user_data['consumer_secret'],$user_data['user_pw']);

// The taxo groups can also be set manually... to avoid to do /taxo_groups each time :)
//Getting all taxo groups for the given database in order to get the sighting updates afterwards
$scope  = '/api/taxo_groups/';
$method = 'GET';
$user_data['request_uri'] = $protocol . $host . $path . $scope;
log_info($user_data['request_uri']);
$raw_taxo_groups = json_decode(curl_request($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw']), true);

if(count($raw_taxo_groups) < 1 || !array_key_exists('data', $raw_taxo_groups)) {
    echo "Error : Couldn't get taxo groups - verify your user data";
    die();
}

log_info("Done.");

//Keeping only open taxo groups
$taxo_groups = array();
log_info("Taxo groups open :");
foreach($raw_taxo_groups['data'] as $t) {
    if($t['access_mode'] != 'none') {
        $taxo_groups[] = $t;
        log_info(" #" . $t['name'] . " (access mode : " . $t['access_mode'] . ")");
    }
}

log_info('.... ' . count($taxo_groups) . ' taxo group(s)');

//Parameters for update /diff API Request
$update_data = json_decode(file_get_contents($filename),1);
$last_update = new DateTime($update_data['last_call']);
$current_update_date = new DateTime(); //Getting the current time to updated later last call date.

$file = $last_update->format('H_i_s_d_m_Y') . "_upd.json";
file_put_contents($folder . $file, ''); // empty the file in case we relaunch the same update


foreach($taxo_groups as $t) {
    log_info("Starting to get diff for taxo group " . $t['id'] . ' - ' . $t['name']);
    // API request params
    $scope = '/api/observations/diff/';
    $method = 'GET';
    $user_data['request_uri'] = $protocol . $host . $path . $scope;

    //Getting all changes from last_call until now
    $get_params = array(
        'id_taxo_group' => $t['id'],
        'modification_type' => 'all',
        'date' => $last_update->format('H:i:s d.m.Y')
    );
    log_info("Get params :  " . implode(',', $get_params));
    $diff = json_decode(curl_request($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw'], "", $get_params), true);
    // $diff contain data like [{"id_sighting":"xx","id_universal":"xx","modification_type":"updated|deleted"}, ...], so we need another request to get the changes of the given id_sightings

    $deleted = array(); $updated = array();
    foreach($diff as $d) {
        //Updated is the state for inserted and modified sightings.
        if($d['modification_type'] == 'updated') {
            //Keeping only the local id_sighting for next API request
            $updated[] = $d['id_sighting'];
        }
        //Deleted sightings
        else if($d['modification_type'] == 'deleted') {
            $deleted[] = $d['id_sighting'];
        }
    }

    $scope = '/api/observations/';
    $method = 'GET';
    $user_data['request_uri'] = $protocol . $host . $path . $scope;

    //Do something with $deleted like deleting in db copy
    /* TODO */

    //Do something with inserted/updated ($updated)

    $chunk_size = 16; // Number of sightings by request

    for($i = 0 ; $i < ceil(count($updated)/$chunk_size) ; $i++) {
        $id_sightings_list = implode(',', array_slice($updated, $i * $chunk_size, $chunk_size));
        log_info("Getting sightings update from ids : " . $id_sightings_list);

        $data = json_decode(curl_request($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw'], '', array('id_sightings_list' => $id_sightings_list, 'id_taxo_group' => $t['id'])), 1);



        foreach($data['data']['sightings'] as $s) {
            //Do something with $data -> store and update in db copy
            file_put_contents($folder . $file, pretty_print_json(json_encode($s)), FILE_APPEND);

        }

        if (isset($data['data']['forms']))
        {
            foreach($data['data']['forms'] as $f) {
                //Do something with $data -> store and update in db copy
                file_put_contents($folder . $file, pretty_print_json(json_encode($f)), FILE_APPEND);
            }
        }
    }

    log_info("Done with taxo group " . $t['id'] . ' - ' . $t['name']);
}

//We are done with getting all the changes, we store the calling date for a next update call
file_put_contents($filename, json_encode(array('last_call' => $current_update_date->format('H:i:s d.m.Y'))));

log_info("End of database copy");