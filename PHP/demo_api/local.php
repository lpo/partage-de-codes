<?php

//API parameters needed to download all the database
$protocol = 'https://'; // usually https:// but can be http://
$host = 'www.faune-***.org';
$path = ""; // must be empty (only for subdomain)

$start_year = 2011; //Downloading data from $start_year
$step_in_days = 15; //In days chunk

$user_data = array(
    'user_pw' => '****',
    'user_email' => '***@***.**',
    'consumer_key' => '*************',
    'consumer_secret' => '*************');

$folder = 'db_copy';
$filename = 'last_update.json'; //File to store last update date
