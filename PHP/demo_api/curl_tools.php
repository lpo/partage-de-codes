<?php

// Log process in db_copy.log
function log_info($text, $log_file = 1) {

    $mem_usage = memory_get_usage(true);
    $mem = '';
    if ($mem_usage < 1024)
        $mem = sprintf("%0.2f  B", $mem_usage);
    elseif ($mem_usage < 5242880)
        $mem = sprintf("%0.4f KB", $mem_usage/1024.0);
    else
        $mem = sprintf("%0.6f MB", $mem_usage/1048576.0);

    $t = microtime(true);
    $micro = sprintf("%06u",($t - floor($t)) * 1000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );

	$msg = $d->format("Y-m-d H:i:s.u") . ' - ' . $mem  . ' - ' . $text .PHP_EOL;

	print $msg;

    if ($log_file)
        file_put_contents('db_sync.log', $msg, FILE_APPEND);
}

function encode_password($consumer_secret,$password)
{
    return "_" . md5(substr($consumer_secret,0,10) . md5($password));
}

function custom_urlencode($s)
{
    return ($s === false) ? $s : str_replace('%7E', '~', rawurlencode($s));
}

function signature($base_string, $consumer_secret, $token_secret)
{
    $key = custom_urlencode($consumer_secret) . '&' . custom_urlencode($token_secret);
    return base64_encode(hash_hmac("sha1", $base_string, $key, true));
}

function build_payload_string($consumer, $user_email, $user_pw, $nonce, $time, $optional_get_params = array())
{
    $sign_method = 'HMAC-SHA1';
    $version = '1.0';

    $post = array(
        'oauth_consumer_key' => $consumer,
        'oauth_nonce' => $nonce,
        'oauth_signature_method' => $sign_method,
        'oauth_timestamp' => $time,
        'oauth_version' => $version,
        'user_email' => $user_email,
        'user_pw' => $user_pw
    );

    if(sizeof($optional_get_params)>0)
        $post = array_merge($optional_get_params, $post);

    ksort($post); // signature params are sorted

    $post_string = '';
    foreach($post as $key => $value)
    {
        $post_string .= $key . '=' . custom_urlencode($value) . '&';
    }

    $post_string = rtrim($post_string, '&');
    return $post_string;
}

function build_authorization_header($consumer, $user_email, $user_pw, $nonce, $time, $signature, $optional_get_params = array())
{
    $sign_method = 'HMAC-SHA1';
    $version = '1.0';

    $post = array(
        'oauth_consumer_key' => $consumer,
        'oauth_nonce' => $nonce,
        'oauth_signature_method' => $sign_method,
        'oauth_timestamp' => $time,
        'oauth_version' => $version,
        'user_email' => $user_email,
        'user_pw' => $user_pw,
        'oauth_signature' => $signature
    );

    if(sizeof($optional_get_params)>0)
        $post = array_merge($optional_get_params, $post);

    ksort($post); // signature params are sorted

    $header_string = '';
    foreach($post as $key => $value)
    {
        $header_string .= $key . '="' . custom_urlencode($value) . '", ';
    }

    $header_string = trim($header_string);
    $header_string = rtrim($header_string, ',');

    return $header_string;
}

function curl_request($method, $host, $uri, $scope, $consumer, $secret, $user_email, $user_pw, $body = "", $optional_get_params = array(), $optional_headers = array(), $server_response_header=false)
{
    $mt = microtime();
    $rand = mt_rand();
    $nonce = md5($mt . $rand);
    $time = time();

    $payload_string = build_payload_string($consumer, $user_email, $user_pw, $nonce, $time, $optional_get_params);

    $base_string = $method . '&' . custom_urlencode($uri) . '&' . custom_urlencode($payload_string);
    $signature = signature($base_string, $secret, false);

    $header = $optional_headers;

    $header[] = $method . ' ' . custom_urlencode($scope) . '&' . custom_urlencode($payload_string) . ' HTTP/1.1';
    $header[] = 'Host: ' . $host;
    $header[] = 'Accept: */*';
    $header[] = 'Authorization: OAuth ' . build_authorization_header($consumer, $user_email, $user_pw, $nonce, $time, $signature, $optional_get_params);
    $header[] = 'Content-Length: ' . strlen($body);
    $header[] = 'Content-Type: application/xml';

    $final_uri = $uri . '?user_pw=' . custom_urlencode($user_pw) . '&user_email=' . custom_urlencode($user_email) . '&oauth_signature=' . $signature;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

    if($method == "POST")
    {
        curl_setopt($ch, CURLOPT_POST, 1);              // send via POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        // add to the url the optional params transmitted, usually none for POST but can be usefull for passing debugging key
        if(sizeof($optional_get_params)>0)
            $final_uri .= "&" . http_build_query($optional_get_params);
    }
    elseif($method == "GET")
    {
        if(sizeof($optional_get_params)>0)
            $final_uri .= "&" . http_build_query($optional_get_params);
    }
    elseif($method == "DELETE")
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    }
    elseif($method == "PUT")
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    }

    curl_setopt($ch, CURLOPT_URL, $final_uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    //curl_setopt($ch, CURLOPT_VERBOSE, true);

    if($server_response_header)
        curl_setopt($ch, CURLOPT_HEADER, true);

    $result = curl_exec($ch);

    if($server_response_header)
    {
        if ($result === false)
        {
            $return = array('header'=>'','body'=>'','error'=> '#' . curl_errno($ch) . '/' . curl_error($ch));
            curl_close($ch);
            return $return;
        }

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        $body = substr($result, $header_size);
        curl_close($ch);
        return array('header'=>$header,'body'=>$body,'error'=>false);
    }
    else
    {
        if ($result === false)
            $result = 'curl error: ' . curl_error($ch);
        curl_close($ch);
        return $result;
    }
}

//This function will iterate through all request using pagination key in order to get ALL the data and saving each result to a file
function curl_request_to_files($method, $host, $uri, $scope, $consumer, $secret, $user_email, $user_pw, $body = "", $optional_get_params = array(), $folder = '')
{
    $_optional_get_params = $optional_get_params;
    $pagination_key_value = '';
    $more_data = true;

    $files = array();

    $i = 0;

    while($more_data)
    {
        $result = curl_request($method, $host, $uri, $scope, $consumer, $secret, $user_email, $user_pw, $body , $_optional_get_params, array(), true);

        $headers = explode("\n",$result['header']);

        foreach($headers as $header_line)
        {
            if (strstr($header_line, 'pagination_key'))
            {
                    list($key,$value)=explode(":",$header_line);
                    if($key=='pagination_key')
                        $pagination_key_value = trim($value);
            }
        }

        if($pagination_key_value=='' || $result['error'])
            $more_data = false;


        if(strlen($result['body'])<50)
            $more_data = false; //data is small we do not need to get more data - end is reached
        else
        {
            $file = $pagination_key_value . "_part_".($i+1).".json";
            file_put_contents($folder . $file, pretty_print_json($result['body']));
            $i++;
            $_optional_get_params = array_merge(array('pagination_key'=>$pagination_key_value),$optional_get_params);
            $pagination_key_value = '';
            $files[] = $file;
        }
    }

    return $files;
}

function pretty_print_json($json)
{
    $result = '';
    $level = 0;
    $prev_char = '';
    $in_quotes = false;
    $ends_line_level = NULL;
    $json_length = strlen($json);

    for( $i = 0; $i < $json_length; $i++ )
    {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";

        if( $ends_line_level !== NULL )
        {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }

        if( $char === '"' && $prev_char != '\\' )
        {
            $in_quotes = !$in_quotes;
        }
        elseif(!$in_quotes)
        {
            switch( $char )
            {
                case '}': case ']':
                $level--;
                $ends_line_level = NULL;
                $new_line_level = $level;
                break;

                case '{': case '[':
                $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                $char = "";
                $ends_line_level = $new_line_level;
                $new_line_level = NULL;
                break;
            }
        }

        if( $new_line_level !== NULL )
        {
            $result .= "\n" . str_repeat("\t", $new_line_level);
        }

        $result .= $char . $post;
        $prev_char = $char;
    }

    return $result;
}

?>