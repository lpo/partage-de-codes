<?php
//Removing time limit because it's going to run for a while.
set_time_limit (0);
ini_set("memory_limit","1G");

header('Content-Type: text/html; charset=utf-8');
include_once ("local.php");
include_once ("curl_tools.php");

if(!is_file('db_sync.log'))
    fopen("db_sync.log", "w") or die("Error : Unable to create log file.");

log_info("************** start db_copy **************");

//Setting up the directories to store json files and log file
if(!is_dir($folder))
    mkdir($folder);

if(!is_dir($folder)) {
    echo "Error : Couldn't set up directory to store json. Please check writing permissions.";
    die();
}

log_info("Getting taxo_groups...");

$user_data['user_pw']  = encode_password($user_data['consumer_secret'],$user_data['user_pw']);
$now = new DateTime();

//Getting all taxo groups for the given database in order to get the sightings afterwards
$scope  = '/api/taxo_groups/';
$method = 'GET';
$user_data['request_uri'] = $protocol . $host . $path . $scope;
log_info($user_data['request_uri']);
$raw_taxo_groups = json_decode(curl_request($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw']), true);

if(count($raw_taxo_groups) < 1 || !array_key_exists('data', $raw_taxo_groups)) {
    log_info("Error : Couldn't get taxo groups");
    die();
}

log_info("Done.");

//Keeping only open taxo groups
$taxo_groups = array();
log_info("Taxo groups open :");
foreach($raw_taxo_groups['data'] as $t) {
    if($t['access_mode'] != 'none') {
        $taxo_groups[] = $t;
        log_info(" #" . $t['name'] . " (access mode : " . $t['access_mode'] . ")");
    }
}

log_info('.... ' . count($taxo_groups) . ' taxo group(s)');

// Request params
$scope = '/api/observations/search/';
$method = 'POST';
$user_data['request_uri'] = $protocol . $host . $path . $scope;
log_info($user_data['request_uri']);
$date_chunk = new DateInterval('P' . $step_in_days . 'D');

//Downloading all history from start_year until now
foreach($taxo_groups as $t) {
    $start_date = new DateTime($start_year . '-01-01 00:00:00');
    $offset_date = new DateTime($start_year . '-01-01 00:00:00');
    $offset_date->add(new DateInterval('P' . ($step_in_days - 1). 'D'));

    log_info("Starting to get data for taxo group " . $t['id'] . ' - ' . $t['name']);
    while($start_date <= $now) {
        $json_body = '{"period_choice":"range","date_from":"' . $start_date->format('d.m.Y') . '","date_to":"' . $offset_date->format('d.m.Y') . '","species_choice":"all", "taxonomic_group":' . $t['id'] . '}';

        log_info($json_body);

        //Saving the result in files for backup possibilities
        curl_request_to_files($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw'], $json_body, array(), $folder.'/');
        $start_date->add($date_chunk);
        $offset_date->add($date_chunk);
    }

    log_info("Done with taxo group " . $t['id'] . ' - ' . $t['name']);
}

// same the end date to be ready for update call
file_put_contents($filename, json_encode(array('last_call' => $now->format('H:i:s d.m.Y'))));

log_info("End of database copy");