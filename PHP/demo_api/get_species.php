<?php

//Setting up to 10 minutes for security
set_time_limit (600);
ini_set("memory_limit","1G");

header('Content-Type: text/html; charset=utf-8');
include_once ("local.php");
include_once ("curl_tools.php");

log_info("************** start species list **************", 0);

$user_data['user_pw']  = encode_password($user_data['consumer_secret'],$user_data['user_pw']);

$scope  = '/api/species';
$method = 'GET';
$user_data['request_uri'] = $protocol . $host . $path . $scope;

log_info($user_data['request_uri'], 0);

$optional_get_params = array();
$optional_headers = array();

// remove the next line if you want to list all species, keeping this line let you get only modified record since the given date
$optional_headers = array("If-Modified-Since: Wed, 13 Sep 2017 00:00:00 GMT");

$pagination_key_value = '';
$more_data = true;

$i = 0;
$c = 0;

while($more_data)
{
    $c++;

    $result = curl_request($method, $host, $user_data['request_uri'], $scope, $user_data['consumer_key'], $user_data['consumer_secret'], $user_data['user_email'], $user_data['user_pw'], '', $optional_get_params, $optional_headers, true);

    if ($result['error'] != '')
    {
        log_info("Error " . $result['error'], 0);
        die();
    }

    $headers = explode("\n",$result['header']);

    foreach($headers as $header_line)
    {
        if (strstr($header_line, 'pagination_key'))
        {
                list($key,$value)=explode(":",$header_line);
                if($key=='pagination_key')
                    $pagination_key_value = trim($value);
        }
    }

    if($pagination_key_value=='' || $result['error'])
        $more_data = false;


    if(strlen($result['body'])<50)
        $more_data = false; //data is small we do not need to get more data - end is reached
    else
    {
        $optional_get_params = array('pagination_key'=>$pagination_key_value);
        $pagination_key_value = '';

        $raw_species = json_decode($result['body'], true);

        foreach($raw_species['data'] as $s)
        {
            $i++;

            $update_info = "";
            if (isset($s['is_deleted'])) // is_deleted and update_date are only returned if If-Modified-Since was part of the initial query
                $update_info = " (is_deleted: " . $s['is_deleted'] . ",last_update: " . $s['update_date'] . ")";

            log_info($s['id'] . " ->  " . $s['latin_name'] . $update_info, 0);
            // uncomment to save the id in a file
            //file_put_contents('species.txt', $s['id'] . "\r\n", FILE_APPEND);
        }
    }
}

log_info("Done $i species found ($c chunks)", 0);





