--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg80+1)
-- Dumped by pg_dump version 10.5 (Debian 10.5-1.pgdg80+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: vm_areas; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vm_areas (
    id integer NOT NULL,
    code character varying(16),
    shape public.geometry(Geometry,3857) NOT NULL,
    type character varying(16) NOT NULL
);


--
-- Name: vm_areas_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vm_areas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vm_areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vm_areas_id_seq OWNED BY public.vm_areas.id;


--
-- Name: vm_observations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vm_observations (
    id_observation bigint NOT NULL,
    cd_nom integer NOT NULL,
    insee character varying(5) NOT NULL,
    dateobs date NOT NULL,
    date_insert date NOT NULL,
    date_update date,
    observateurs text NOT NULL,
    donnee_publique boolean NOT NULL,
    id_source integer,
    validation boolean NOT NULL,
    the_geom_point public.geometry(Point,3857),
    geojson_point text,
    effectif_total integer,
    altitude_retenue integer,
    diffusable boolean
);


--
-- Name: vm_sources; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vm_sources (
    id smallint NOT NULL,
    name character varying(64),
    is_used boolean
);


--
-- Name: vm_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vm_sources_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vm_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vm_sources_id_seq OWNED BY public.vm_sources.id;


--
-- Name: vm_species_sheet; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vm_species_sheet (
    id integer NOT NULL,
    biolovision_id integer NOT NULL,
    cd_nom integer,
    latin_name character varying(64) NOT NULL
);


--
-- Name: vm_species_sheet_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vm_species_sheet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vm_species_sheet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vm_species_sheet_id_seq OWNED BY public.vm_species_sheet.id;


--
-- Name: vm_areas id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_areas ALTER COLUMN id SET DEFAULT nextval('public.vm_areas_id_seq'::regclass);


--
-- Name: vm_sources id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_sources ALTER COLUMN id SET DEFAULT nextval('public.vm_sources_id_seq'::regclass);


--
-- Name: vm_species_sheet id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_species_sheet ALTER COLUMN id SET DEFAULT nextval('public.vm_species_sheet_id_seq'::regclass);


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: vm_species_sheet; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.vm_species_sheet (id, biolovision_id, cd_nom, latin_name) FROM stdin;
1	7250	77381	Emys orbicularis (Linnaeus, 1758)
2	7260	77424	Trachemys scripta (Schoepff, 1792)
3	7440	77600	Lacerta agilis Linnaeus, 1758
4	7460	77619	Lacerta bilineata Daudin, 1802
5	7490	77756	Podarcis muralis (Laurenti, 1768)
6	7470	79278	Zootoca vivipara (Lichtenstein, 1823)
7	7300	77490	Anguis fragilis Linnaeus, 1758
8	7340	78048	Natrix maura (Linnaeus, 1758)
9	7320	77955	Coronella austriaca Laurenti, 1768
10	7310	444446	Zamenis longissimus (Laurenti, 1768)
11	7550	78130	Vipera aspis (Linnaeus, 1758)
12	7560	78141	Vipera berus (Linnaeus, 1758)
13	8950	444430	Ichthyosaura alpestris (Laurenti, 1768)
14	8970	444432	Lissotriton helveticus (Razoumowsky, 1789)
15	15019	444431	Lissotriton vulgaris (Linnaeus, 1758)
16	8990	92	Salamandra salamandra (Linnaeus, 1758)
17	8980	139	Triturus cristatus (Laurenti, 1768)
18	9020	163	Triturus marmoratus (Latreille, 1800)
19	8620	197	Alytes obstetricans (Laurenti, 1768)
20	8630	212	Bombina variegata (Linnaeus, 1758)
21	8710	235	Pelobates cultripes (Cuvier, 1829)
22	8720	252	Pelodytes punctatus (Daudin, 1803)
23	8730	79265	Xenopus laevis (Daudin, 1803)
24	15294	774678	Bufo spinosus Daudin, 1803
25	8640	774678	Bufo spinosus Daudin, 1803
26	8650	459628	Epidalea calamita (Laurenti, 1768)
27	8670	281	Hyla arborea (Linnaeus, 1758)
28	8680	292	Hyla meridionalis Boettger, 1874
29	8790	444441	Pelophylax lessonae (Camerano, 1882)
30	8770	444440	Pelophylax kl. esculentus (Linnaeus, 1758)
31	8810	444443	Pelophylax ridibundus (Pallas, 1771)
32	8800	444442	Pelophylax perezi (Seoane, 1885)
33	8780	444439	Pelophylax kl. grafi (Crochet, Dubois, Ohler & Tunner, 1995)
34	8830	310	Rana dalmatina Fitzinger in Bonaparte, 1838
35	8870	351	Rana temporaria Linnaeus, 1758
36	3370	60015	Erinaceus europaeus Linnaeus, 1758
37	5890	60102	Sorex coronatus Millet, 1828
38	5880	60038	Sorex minutus Linnaeus, 1766
39	5840	60127	Neomys fodiens (Pennant, 1771)
40	5830	60119	Neomys anomalus Cabrera, 1907
41	5820	60205	Crocidura russula (Hermann, 1780)
42	5800	60176	Crocidura leucodon (Hermann, 1780)
43	5810	60187	Crocidura suaveolens (Pallas, 1811)
44	5850	60237	Suncus etruscus (Savi, 1822)
45	5910	60249	Talpa europaea (Linnaeus, 1758)
46	6560	60295	Rhinolophus ferrumequinum (Schreber, 1774)
47	6520	60313	Rhinolophus hipposideros (Bechstein, 1800)
48	6530	60330	Rhinolophus euryale Blasius, 1853
49	6640	79305	Miniopterus schreibersii (Kuhl, 1817)
50	6610	60360	Eptesicus serotinus (Schreber, 1774)
51	6790	60468	Nyctalus noctula (Schreber, 1774)
52	6800	60461	Nyctalus leisleri (Kuhl, 1817)
53	6850	60479	Pipistrellus pipistrellus (Schreber, 1774)
54	6870	60489	Pipistrellus pygmaeus (Leach, 1825)
55	6860	60490	Pipistrellus nathusii (Keyserling & Blasius, 1839)
56	6840	79303	Pipistrellus kuhlii (Kuhl, 1817)
57	6580	60345	Barbastella barbastellus (Schreber, 1774)
58	6910	60518	Plecotus auritus (Linnaeus, 1758)
59	6880	60527	Plecotus austriacus (J.B. Fischer, 1829)
60	6930	60537	Vespertilio murinus Linnaeus, 1758
61	6650	200118	Myotis daubentonii (Kuhl, 1817)
62	6660	79299	Myotis alcathoe Helversen & Heller, 2001
63	6730	60383	Myotis mystacinus (Kuhl, 1817)
64	6720	60400	Myotis emarginatus (E. Geoffroy, 1806)
65	6760	60408	Myotis nattereri (Kuhl, 1817)
66	6680	79301	Myotis bechsteinii (Kuhl, 1817)
67	6670	60418	Myotis myotis (Borkhausen, 1797)
68	3330	60822	Procyon lotor (Linnaeus, 1758)
69	3180	60582	Nyctereutes procyonoides (Gray, 1834)
70	3190	60585	Vulpes vulpes (Linnaeus, 1758)
71	3240	60658	Martes martes (Linnaeus, 1758)
72	3250	60674	Martes foina (Erxleben, 1777)
73	3310	60716	Mustela nivalis Linnaeus, 1766
74	3290	60686	Mustela erminea Linnaeus, 1758
75	3320	60746	Mustela vison Schreber, 1777
76	3280	60704	Mustela lutreola (Linnaeus, 1761)
77	3270	60731	Mustela putorius Linnaeus, 1758
78	3260	199987	Meles meles (Linnaeus, 1758)
79	3230	60630	Lutra lutra (Linnaeus, 1758)
80	3350	60831	Genetta genetta (Linnaeus, 1758)
81	3220	79306	Felis silvestris Schreber, 1775
82	3140	60981	Sus scrofa Linnaeus, 1758
83	3120	61028	Dama dama (Linnaeus, 1758)
84	3110	61000	Cervus elaphus Linnaeus, 1758
85	3100	61057	Capreolus capreolus (Linnaeus, 1758)
86	3380	61678	Lepus europaeus Pallas, 1778
87	3410	61714	Oryctolagus cuniculus (Linnaeus, 1758)
88	3480	61153	Sciurus vulgaris Linnaeus, 1758
89	3440	61212	Castor fiber Linnaeus, 1758
90	6180	61648	Glis glis (Linnaeus, 1766)
91	6170	61618	Eliomys quercinus (Linnaeus, 1766)
92	6190	61636	Muscardinus avellanarius (Linnaeus, 1758)
93	6020	61290	Clethrionomys glareolus (Schreber, 1780)
94	6000	61258	Arvicola sapidus Miller, 1908
95	5990	61398	Microtus gerbei (Gerbe, 1879)
96	6060	61379	Microtus arvalis (Pallas, 1778)
97	6050	61357	Microtus agrestis (Linnaeus, 1761)
98	6140	61448	Ondatra zibethicus (Linnaeus, 1766)
99	6040	61543	Micromys minutus (Pallas, 1771)
100	5960	61498	Apodemus flavicollis (Melchior, 1834)
101	5950	61510	Apodemus sylvaticus (Linnaeus, 1758)
102	6160	61587	Rattus rattus (Linnaeus, 1758)
103	6150	61585	Rattus norvegicus (Berkenhout,1769)
104	6120	61568	Mus musculus Linnaeus, 1758
105	3450	61667	Myocastor coypus (Molina, 1782)
106	55	2702	Cygnus atratus
107	54	2706	Cygnus olor
108	53	2709	Cygnus columbianus
109	52	2715	Cygnus cygnus
110	63	2720	Anser fabalis
111	65	2725	Anser brachyrhynchus
112	61	2734	Anser albifrons
113	62	2739	Anser erythropus
114	60	2741	Anser anser
115	72	2747	Branta canadensis
116	71	2750	Branta leucopsis
117	70	2757	Branta bernicla
118	73	2753	Branta ruficollis
119	81	2763	Alopochen aegyptiacus
120	110	2775	Aix sponsa
121	82	2767	Tadorna tadorna
122	109	2776	Aix galericulata
123	106	1952	Mareca penelope
124	107	1955	Mareca americana
125	111	836203	Mareca strepera
126	94	1958	Anas crecca
127	96	1961	Anas crecca carolinensis
128	86	1966	Anas platyrhynchos
129	101	1973	Anas acuta
130	92	836222	Spatula querquedula
131	90	836223	Spatula discors
132	112	1972	Spatula clypeata
133	105	1980	Marmaronetta angustirostris
134	115	1984	Netta rufina
135	118	1991	Aythya ferina
136	120	1988	Aythya collaris
137	121	1995	Aythya nyroca
138	119	1998	Aythya fuligula
139	123	2001	Aythya marila
140	124	199305	Aythya affinis
141	131	2005	Somateria mollissima
142	129	2790	Clangula hyemalis
143	133	2794	Melanitta nigra
144	1079	199308	Melanitta americana
145	1081	2798	Melanitta perspicillata
146	134	2801	Melanitta fusca
147	126	2808	Bucephala clangula
148	140	199312	Mergellus albellus
149	142	2816	Mergus serrator
150	141	2818	Mergus merganser
151	137	2823	Oxyura jamaicensis
152	136	2826	Oxyura leucocephala
153	189	2996	Coturnix coturnix
154	186	2975	Alectoris rufa
155	188	2989	Perdix perdix
156	1127	3000	Syrmaticus reevesii
157	190	3003	Phasianus colchicus
158	1	2411	Gavia stellata
159	2	956	Gavia arctica
160	3	959	Gavia immer
161	10	998	Fulmarus glacialis
162	11	1009	Calonectris diomedea
163	12	790987	Ardenna gravis
164	13	1024	Puffinus griseus
165	14	1027	Puffinus puffinus
166	1107	199318	Puffinus mauretanicus
167	15	1031	Puffinus yelkouan
168	16	836253	Puffinus lherminieri baroli
169	1084	1039	Oceanites oceanicus
170	17	2419	Hydrobates pelagicus
171	18	790985	Hydrobates castro
172	19	790986	Hydrobates leucorhous
173	23	2437	Morus bassanus
174	24	2440	Phalacrocorax carbo
175	27	2447	Phalacrocorax aristotelis
176	38	2473	Botaurus stellaris
177	37	2477	Ixobrychus minutus
178	36	2481	Nycticorax nycticorax
179	31	2486	Ardeola ralloides
180	32	2489	Bubulcus ibis
181	35	2494	Egretta gularis
182	34	2497	Egretta garzetta
183	33	2504	Ardea alba
184	29	2506	Ardea cinerea
185	30	2508	Ardea purpurea
186	41	2514	Ciconia nigra
187	39	2517	Ciconia ciconia
188	44	2522	Plegadis falcinellus
189	588	2687	Threskiornis aethiopicus
190	42	2530	Platalea leucorodia
191	1104	982	Podilymbus podiceps
192	5	977	Tachybaptus ruficollis
193	8	965	Podiceps cristatus
194	9	968	Podiceps grisegena
195	6	971	Podiceps auritus
196	7	974	Podiceps nigricollis
197	144	2832	Pernis apivorus
198	143	2836	Elanus caeruleus
199	146	2840	Milvus migrans
200	145	2844	Milvus milvus
201	158	2848	Haliaeetus albicilla
202	160	2860	Gyps fulvus
203	168	2873	Circaetus gallicus
204	166	2878	Circus aeruginosus
205	163	2881	Circus cyaneus
206	164	2884	Circus macrourus
207	165	2887	Circus pygargus
208	147	2891	Accipiter gentilis
209	148	2895	Accipiter nisus
210	150	2623	Buteo buteo
211	149	2627	Buteo rufinus
212	151	2630	Buteo lagopus
213	156	836346	Clanga clanga
214	157	836345	Clanga pomarina
215	153	2651	Hieraaetus pennatus
216	154	2645	Aquila chrysaetos
217	152	2657	Aquila fasciata
218	169	2660	Pandion haliaetus
219	196	3036	Rallus aquaticus
220	200	3039	Porzana porzana
221	198	836245	Zapornia parva
222	199	836246	Zapornia pusilla
223	197	3053	Crex crex
224	202	3059	Gallinula chloropus
225	203	3065	Porphyrio alleni
226	205	3070	Fulica atra
227	193	3076	Grus grus
228	206	3089	Tetrax tetrax
229	207	3101	Otis tarda
230	259	3120	Burhinus oedicnemus
231	254	3112	Himantopus himantopus
232	255	3116	Recurvirostra avosetta
233	209	3106	Haematopus ostralegus
234	215	3165	Pluvialis squatarola
235	216	3161	Pluvialis apricaria
236	1102	3158	Pluvialis dominica
237	590	3170	Pluvialis fulva
238	221	3153	Eudromias morinellus
239	220	3144	Charadrius vociferus
240	11321	199364	Charadrius semipalmatus
241	217	3140	Charadrius hiaticula
242	218	3136	Charadrius dubius
243	211	3178	Chettusia gregaria
244	213	3187	Vanellus vanellus
245	1035	3150	Charadrius asiaticus
246	1037	3146	Charadrius mongolus
247	1038	199365	Charadrius pecuarius
248	219	3142	Charadrius alexandrinus
249	222	2571	Numenius phaeopus
250	224	2576	Numenius arquata
251	225	2563	Limosa limosa
252	226	2568	Limosa lapponica
253	236	3239	Arenaria interpres
254	242	3192	Calidris canutus
255	253	814245	Calidris pugnax
256	1022	3231	Calidris acuminata
257	251	828816	Calidris falcinellus
258	250	2901	Calidris ferruginea
259	1023	444423	Calidris himantopus
260	1027	199353	Calidris ruficollis
261	1707	10002	Calidris subminuta
262	244	3210	Calidris temminckii
263	241	3195	Calidris alba
264	249	2911	Calidris alpina
265	248	2906	Calidris maritima
266	246	3222	Calidris bairdii
267	243	3206	Calidris minuta
268	245	3218	Calidris fuscicollis
269	1025	3214	Calidris minutilla
270	252	2929	Calidris subruficollis
271	247	3226	Calidris melanotos
272	1024	3203	Calidris mauri
273	1026	3199	Calidris pusilla
274	1092	3249	Steganopus tricolor
275	257	3243	Phalaropus lobatus
276	256	3250	Phalaropus fulicarius
277	235	2610	Xenus cinereus
278	233	2616	Actitis hypoleucos
279	234	459460	Actitis macularius
280	231	2603	Tringa ochropus
281	227	2584	Tringa erythropus
282	230	2594	Tringa nebularia
283	1130	2597	Tringa melanoleuca
284	1033	626151	Tringa semipalmata
285	229	2591	Tringa stagnatilis
286	232	2607	Tringa glareola
287	228	2586	Tringa totanus
288	239	2538	Lymnocryptes minimus
289	1073	2557	Limnodromus griseus
290	1074	2554	Limnodromus scolopaceus
291	240	2559	Scolopax rusticola
292	238	2543	Gallinago gallinago
293	237	2549	Gallinago media
294	261	3129	Glareola pratincola
295	262	626275	Glareola nordmanni
296	264	3255	Stercorarius pomarinus
297	265	3258	Stercorarius parasiticus
298	266	3261	Stercorarius longicaudus
299	263	3263	Stercorarius skua
300	304	3402	Fratercula arctica
301	302	3392	Cepphus grylle
302	300	3388	Alca torda
303	299	3396	Alle alle
304	301	3379	Uria aalge
305	1116	532691	Onychoprion anaethetus
306	296	3352	Sternula albifrons
307	291	3332	Gelochelidon nilotica
308	292	3336	Hydroprogne caspia
309	288	459627	Chlidonias hybrida
310	290	3371	Chlidonias niger
311	289	3374	Chlidonias leucopterus
312	298	3362	Thalasseus sandvicensis
313	1117	3356	Thalasseus elegans
314	297	3359	Thalasseus bengalensis
315	1118	199377	Sterna forsteri
316	293	3343	Sterna hirundo
317	295	3364	Sterna dougallii
318	294	3345	Sterna paradisaea
319	287	364251	Xema sabini
320	286	3318	Rissa tridactyla
321	283	534662	Chroicocephalus genei
322	1070	534682	Chroicocephalus philadelphia
323	282	530157	Chroicocephalus ridibundus
324	284	534748	Hydrocoloeus minutus
325	285	3314	Rhodostethia rosea
326	580	627332	Leucophaeus atricilla
327	591	627334	Leucophaeus pipixcan
328	281	627745	Ichthyaetus melanocephalus
329	269	627743	Ichthyaetus audouinii
330	270	3293	Larus canus
331	280	3278	Larus delawarensis
332	275	3297	Larus fuscus
333	272	3302	Larus argentatus
334	273	199374	Larus michahellis
335	274	3289	Larus cachinnans
336	1071	534749	Larus smithsonianus
337	279	3307	Larus glaucoides
338	278	3309	Larus hyperboreus
339	276	3311	Larus marinus
340	308	10003	"Columba livia ""urbicum"" "
341	309	3422	Columba oenas
342	310	3424	Columba palumbus
343	312	3429	Streptopelia decaocto
344	311	3439	Streptopelia turtur
345	1121	3442	Streptopelia orientalis
346	313	3461	Clamator glandarius
347	314	3465	Cuculus canorus
348	315	3482	Tyto alba
349	316	3489	Otus scops
350	320	3511	Athene noctua
351	321	3518	Strix aluco
352	322	3522	Asio otus
353	323	3525	Asio flammeus
354	325	3540	Caprimulgus europaeus
355	327	3551	Apus apus
356	328	3555	Apus pallidus
357	326	3561	Tachymarptis melba
358	1009	199399	Apus affinis
359	332	3590	Upupa epops
360	330	3582	Merops apiaster
361	331	3586	Coracias garrulus
362	329	3571	Alcedo atthis
363	333	3595	Jynx torquilla
364	335	3601	Picus canus
365	334	3603	Picus viridis
366	336	3608	Dryocopus martius
367	338	3619	Dendrocopos medius
368	337	3611	Dendrocopos major
369	340	3630	Dendrocopos minor
370	178	2666	Falco naumanni
371	179	2669	Falco tinnunculus
372	177	2674	Falco vespertinus
373	176	2676	Falco columbarius
374	174	2679	Falco subbuteo
375	175	2681	Falco eleonorae
376	170	2683	Falco biarmicus
377	172	2931	Falco rusticolus
378	173	2938	Falco peregrinus
379	1137	4560	Vireo olivaceus
380	356	3803	Oriolus oriolus
381	1065	199406	Lanius cristatus
382	490	3807	Lanius collurio
383	488	3811	Lanius minor
384	486	3814	Lanius excubitor
385	487	199409	Lanius meridionalis
386	489	4460	Lanius senator
387	368	4488	Pyrrhocorax pyrrhocorax
388	363	4474	Pica pica
389	366	4466	Garrulus glandarius
390	365	4480	Nucifraga caryocatactes
391	362	4494	Corvus monedula
392	1044	199412	Corvus dauuricus
393	361	4501	Corvus frugilegus
394	358	4503	Corvus corone
395	360	4505	Corvus corone cornix
396	357	4510	Corvus corax
397	455	4308	Regulus regulus
398	456	459638	Regulus ignicapilla
399	378	3798	Remiz pendulinus
400	371	534742	Cyanistes caeruleus
401	370	3764	Parus major
402	374	534750	Lophophanes cristatus
403	373	534751	Periparus ater
404	376	4355	Parus montanus
405	375	534753	Poecile palustris
406	379	4338	Panurus biarmicus
407	348	3670	Lullula arborea
408	349	3676	Alauda arvensis
409	347	3656	Galerida cristata
410	350	3681	Eremophila alpestris
411	345	3649	Calandrella brachydactyla
412	343	3644	Melanocorypha calandra
413	355	3688	Riparia riparia
414	353	3692	Ptyonoprogne rupestris
415	351	3696	Hirundo rustica
416	354	459478	Delichon urbicum
417	352	3701	Cecropis daurica
418	416	4151	Cettia cetti
419	377	4342	Aegithalos caudatus
420	1099	4267	Phylloscopus proregulus
421	452	4297	Phylloscopus inornatus
422	1096	199473	Phylloscopus humei
423	1100	4300	Phylloscopus schwarzi
424	451	4304	Phylloscopus fuscatus
425	449	4269	Phylloscopus bonelli
426	450	4272	Phylloscopus sibilatrix
427	446	4280	Phylloscopus collybita
428	1321	199477	Phylloscopus ibericus
429	445	4289	Phylloscopus trochilus
430	433	4257	Sylvia atricapilla
431	436	4254	Sylvia borin
432	1517	4239	Sylvia nana
433	434	4245	Sylvia nisoria
434	438	4247	Sylvia curruca
435	439	4232	Sylvia melanocephala
436	440	4229	Sylvia cantillans
437	437	4252	Sylvia communis
438	441	4227	Sylvia conspicillata
439	442	4221	Sylvia undata
440	417	4167	Locustella naevia
441	419	4172	Locustella luscinioides
442	432	199465	Iduna caligata
443	1061	199464	Iduna rama
444	429	4212	Hippolais icterina
445	428	4215	Hippolais polyglotta
446	427	4184	Acrocephalus paludicola
447	426	4187	Acrocephalus schoenobaenus
448	425	199459	Acrocephalus agricola
449	424	4190	Acrocephalus dumetorum
450	423	4192	Acrocephalus palustris
451	422	4195	Acrocephalus scirpaceus
452	421	4198	Acrocephalus arundinaceus
453	444	4155	Cisticola juncidis
454	485	3953	Bombycilla garrulus
455	383	3780	Tichodroma muraria
456	380	3774	Sitta europaea
457	381	3784	Certhia familiaris
458	382	3791	Certhia brachydactyla
459	385	3967	Troglodytes troglodytes
460	1124	4518	Sturnus unicolor
461	491	4516	Sturnus vulgaris
462	492	4520	Sturnus roseus
463	1031	4106	Catharus minimus
464	406	199434	Zoothera aurea
465	407	4112	Turdus torquatus
466	408	4117	Turdus merula
467	411	4123	Turdus naumanni
468	410	4134	Turdus atrogularis
469	412	4127	Turdus pilaris
470	414	4129	Turdus philomelos
471	413	4137	Turdus iliacus
472	415	4142	Turdus viscivorus
473	457	4319	Muscicapa striata
474	386	4001	Erithacus rubecula
475	388	4013	Luscinia megarhynchos
476	389	4023	Luscinia svecica
477	392	4030	Tarsiger cyanurus
478	460	4324	Ficedula parva
479	459	4327	Ficedula albicollis
480	458	4330	Ficedula hypoleuca
481	394	4035	Phoenicurus ochruros
482	395	4040	Phoenicurus phoenicurus
483	403	4084	Monticola saxatilis
484	396	4049	Saxicola rubetra
485	1111	836856	Saxicola maurus
486	397	199425	Saxicola rubicola
487	399	4064	Oenanthe oenanthe
488	401	4074	Oenanthe hispanica
489	400	199428	Oenanthe deserti
490	462	3984	Prunella collaris
491	461	3978	Prunella modularis
492	493	4525	Passer domesticus
493	495	4532	Passer montanus
494	497	4540	Petronia petronia
495	475	3741	Motacilla flava
496	484	3948	Motacilla citreola
497	474	3755	Motacilla cinerea
498	472	3941	Motacilla alba
499	464	3709	Anthus richardi
500	465	3713	Anthus campestris
501	467	3723	Anthus trivialis
502	1008	3738	Anthus gustavi
503	463	3726	Anthus pratensis
504	468	3729	Anthus cervinus
505	1280	3716	Anthus petrosus
506	469	3733	Anthus spinoletta
507	519	4568	Fringilla montifringilla
508	518	4564	Fringilla coelebs
509	498	4625	Coccothraustes coccothraustes
510	512	4616	Carpodacus erythrinus
511	511	4619	Pyrrhula pyrrhula
512	576	2405	Bucanetes githagineus
513	499	4580	Carduelis chloris
514	502	4588	Carduelis cannabina
515	503	4590	Carduelis flavirostris
516	505	4595	Carduelis flammea
517	517	4609	Loxia leucoptera
518	515	4603	Loxia curvirostra
519	500	4583	Carduelis carduelis
520	507	4576	Carduelis citrinella
521	508	4571	Serinus serinus
522	501	4586	Carduelis spinus
523	533	4649	Plectrophenax nivalis
524	532	4644	Calcarius lapponicus
525	523	4680	Emberiza melanocephala
526	520	4686	Emberiza calandra
527	528	4663	Emberiza cia
528	526	4659	Emberiza cirlus
529	527	4665	Emberiza hortulana
530	521	4657	Emberiza citrinella
531	531	4669	Emberiza schoeniclus
532	525	4678	Emberiza aureola
533	530	4667	Emberiza pusilla
534	529	4676	Emberiza rustica
535	473	3941	Motacilla alba
536	1182	3941	Motacilla alba
537	1508	3941	Motacilla alba
538	504	4595	Carduelis flammea
539	1161	3302	Larus argentatus
540	6080	61425	Microtus subterraneus (de Sélys-Longchamps, 1836)
546	7290	77433	Testudo hermanni Gmelin, 1789
547	15111	851674	Natrix helvetica (Lacepède, 1789)
548	7370	77949	Hierophis viridiflavus (Lacepède, 1789)
549	15295	774678	Bufo spinosus Daudin, 1803
550	9040	150	Triturus cristatus x T. marmoratus
551	1162	3297	Larus fuscus Linnaeus, 1758
552	1160	3302	Larus argentatus Pontoppidan, 1763
553	1371	2760	Branta bernicla bernicla (Linnaeus, 1758) 
554	116	813759	Netta peposaca (Vieillot, 1816)
555	476	3741	Motacilla flava Linnaeus, 1758
556	1351	3741	Motacilla flava Linnaeus, 1758
557	77	2770	Tadorna ferruginea
558	1016	2762	Branta bernicla hrota
559	69	2731	Anser indicus
560	1122	3432	Streptopelia roseogrisea
561	537	3448	Psittacula krameri
562	1424	3482	Tyto alba
563	25	2440	Phalacrocorax carbo
564	477	3745	Motacilla flava flavissima
565	1180	4285	Phylloscopus collybita collyba
566	1234	4028	Luscinia svecica namnetum
567	15154	60595	Felis catus
568	7360	851674	Natrix natrix
569	1494	4564	Fringilla coelebs
570	26	2446	Phalacrocorax carbo sinensis 
571	77	2770	Tadorna ferruginea
\.


--
-- Name: vm_areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.vm_areas_id_seq', 5264, true);


--
-- Name: vm_sources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.vm_sources_id_seq', 4, true);


--
-- Name: vm_species_sheet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.vm_species_sheet_id_seq', 571, true);


--
-- Name: vm_areas vm_areas_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_areas
    ADD CONSTRAINT vm_areas_pkey PRIMARY KEY (id);


--
-- Name: vm_observations vm_observations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_observations
    ADD CONSTRAINT vm_observations_pkey PRIMARY KEY (id_observation);


--
-- Name: vm_sources vm_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_sources
    ADD CONSTRAINT vm_sources_pkey PRIMARY KEY (id);


--
-- Name: vm_species_sheet vm_species_sheet_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_species_sheet
    ADD CONSTRAINT vm_species_sheet_pkey PRIMARY KEY (id);


--
-- Name: vm_observations vm_observation_source_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vm_observations
    ADD CONSTRAINT vm_observation_source_fk FOREIGN KEY (id_source) REFERENCES public.vm_sources(id);


--
-- PostgreSQL database dump complete
--

